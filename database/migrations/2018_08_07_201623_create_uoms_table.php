<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //unity meassure
        Schema::create('uoms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('uoms_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->text('presentation')->nullable();
            $table->unsignedInteger('uom_id');
            $table->string('locale')->index();
            $table->unique(['uom_id','locale']);

        });
        Schema::table('uoms_translations', function (Blueprint $table) {
            $table->foreign('uom_id')
                ->references('id')->on('uoms')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uoms_translations');
        Schema::dropIfExists('uoms');
    }
}
