<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('status',['active','hide','inactive'])->default('active');
            $table->unsignedInteger('family_id');
            $table->string('avatar')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('categories_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->text('short_description')->nullable();
            $table->text('description')->nullable();
            $table->unsignedInteger('category_id');
            $table->string('locale')->index();
            $table->unique(['category_id','locale']);

        });
        Schema::table('categories', function (Blueprint $table) {
            $table->foreign('family_id')
                ->references('id')->on('families')
                ->onDelete('cascade');
        });
        Schema::table('categories_translations', function (Blueprint $table) {
            $table->foreign('category_id')
                ->references('id')->on('categories')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories_translations');
        Schema::dropIfExists('categories');
    }
}
