<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sku')->nullable();
            $table->unsignedInteger('uom_id');
            $table->unsignedInteger('brand_id');
            $table->string('avatar')->nullable();
            $table->string('avatars')->nullable();
            $table->enum('status',['active','hide','inactive'])->default('active');
            $table->datetime('available_on')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('products_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedInteger('product_id');
            $table->string('locale')->index();
            $table->string('slug');
            $table->text('short_description')->nullable();
            $table->text('description')->nullable();
            $table->unique(['product_id','locale']);
        });
        Schema::table('products', function (Blueprint $table) {
            $table->foreign('uom_id')
                ->references('id')->on('uoms')
                ->onDelete('cascade');
            $table->foreign('brand_id')
                ->references('id')->on('brands')
                ->onDelete('cascade');
        });
        Schema::table('products_translations', function (Blueprint $table) {
            $table->foreign('product_id')
                ->references('id')->on('products')
                ->onDelete('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('products_translations');
        Schema::dropIfExists('products');
    }
}
