<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_options', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedInteger('product_id');
            $table->mediumInteger('sort_order')->unsigned()->nullable();
            $table->boolean('is_required')->default('1');
            $table->boolean('is_hidden')->default('0');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('product_options_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_option_id');
            $table->string('locale')->index();
            $table->string('presentation');
            $table->unique(['product_option_id','locale']);
        });
        Schema::table('product_options', function (Blueprint $table) {
            $table->foreign('product_id')
                ->references('id')->on('products')
                ->onDelete('cascade');
        });
        Schema::table('product_options_translations', function (Blueprint $table) {
            $table->foreign('product_option_id')
                ->references('id')->on('product_options')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_options_translations');
        Schema::dropIfExists('product_options');
    }
}
