<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_variants', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('master')->default(0);
            $table->string('name')->nullable();
            $table->string('sku')->unique();
            $table->integer('on_hand')->nullable();
            $table->boolean('status')->default('1');
            $table->boolean('available_on_demand')->default('1');
            $table->decimal('weight')->nullable();
            $table->decimal('width')->nullable();
            $table->decimal('height')->nullable();
            $table->decimal('depth')->nullable();
            $table->unsignedInteger('product_id');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('product_variant_option_value', function (Blueprint $table) {
            $table->unsignedInteger('product_variant_id');
            $table->unsignedInteger('product_option_value_id');
        });
        Schema::table('product_variants', function (Blueprint $table) {
            $table->foreign('product_id')
                ->references('id')->on('products')
                ->onDelete('cascade');
        });
        Schema::table('product_variant_option_value', function (Blueprint $table) {
            $table->foreign('product_variant_id')
                ->references('id')->on('product_variants')
                ->onDelete('cascade');
            $table->foreign('product_option_value_id')
                ->references('id')->on('product_option_values');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_variant_option_value');
        Schema::dropIfExists('product_variants');
    }
}
