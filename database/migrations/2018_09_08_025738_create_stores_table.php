<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('address')->nullable();
            $table->boolean('status')->default('1')->nullable();
            $table->unsignedInteger('store_area_id');
            $table->unsignedInteger('supplier_id');
            $table->text('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('store_product_variant', function (Blueprint $table) {

            $table->unsignedInteger('product_variant_id');
            $table->unsignedInteger('store_id');
            $table->integer('on_hand')->default(0);
            $table->boolean('status')->default('1');
            $table->boolean('available_on_demand')->default(1);
            $table->bigInteger('price');
            $table->dateTime('start_date');
            $table->dateTime('end_date')->nullabe();
            $table->text('description')->nullable();
            $table->timestamps();
        });

        Schema::table('stores', function (Blueprint $table) {
            $table->foreign('store_area_id')
                ->references('id')->on('store_areas')
                ->onDelete('cascade');
            $table->foreign('supplier_id')
                ->references('id')->on('suppliers')
                ->onDelete('cascade');
        });
        Schema::table('store_product_variant', function (Blueprint $table) {
            $table->foreign('product_variant_id')
                ->references('id')->on('product_variants')
                ->onDelete('cascade');
            $table->foreign('store_id')
                ->references('id')->on('stores')
                ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
