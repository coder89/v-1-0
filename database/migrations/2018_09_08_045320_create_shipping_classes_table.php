<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_classes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->boolean('status')->default('1');
            $table->enum('added_value_rule',['smallest','biggest'])->default('biggest');
            $table->bigInteger('added_value_fixed')->default(0);
            $table->decimal('added_percentage',6,2)->default(0);
            $table->text('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('shipping_costs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedInteger('shipping_class_id');
            $table->unsignedInteger('store_area_id');
            $table->unsignedInteger('area_id');
            $table->unsignedInteger('shipping_agent_id')->nullable();
            $table->dateTime('start_date');
            $table->bigInteger('cost')->default(0);

            $table->dateTime('end_date')->nullable();
            $table->boolean('status')->default(1);
            $table->text('description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('shipping_agent_shipping_class', function (Blueprint $table) {
            $table->unsignedInteger('shipping_agent_id');
            $table->unsignedInteger('shipping_class_id');

        });


        Schema::table('shipping_costs', function (Blueprint $table) {
            $table->foreign('shipping_class_id')
                ->references('id')->on('shipping_classes')
                ->onDelete('cascade');
            $table->foreign('store_area_id')
                ->references('id')->on('store_areas');
            $table->foreign('shipping_agent_id')
                ->references('id')->on('shipping_agents');
        });

        Schema::table('shipping_agent_shipping_class', function (Blueprint $table) {
            $table->foreign('shipping_agent_id')
                ->references('id')->on('shipping_agents')
                ->onDelete('cascade');
            $table->foreign('shipping_class_id')
                ->references('id')->on('shipping_classes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_classes');
        Schema::dropIfExists('shipping_costs');
        Schema::dropIfExists('shipping_agent_shipping_class');
    }
}
