<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_properties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });
        Schema::create('product_properties_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('property_id');
            $table->string('locale')->index();
            $table->string('presentation');
            $table->unique(['property_id','locale']);
        });
        Schema::table('product_properties_translations', function (Blueprint $table) {
            $table->foreign('property_id')
                ->references('id')->on('product_properties')
                ->onDelete('cascade');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_properties_translations');
        Schema::dropIfExists('product_properties');
    }
}