<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brands', function (Blueprint $table) {
            $table->increments('id');
            $table->string('avatar')->nullable();
            $table->enum('status',['active','hide','inactive'])->default('active');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('brands_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('slug');
            $table->string('manufacturer')->nullable();
            $table->text('short_description')->nullable();
            $table->text('description')->nullable();
            $table->unsignedInteger('brand_id');
            $table->string('locale')->index();
            $table->unique(['brand_id','locale']);
        });
        Schema::table('brands_translations', function (Blueprint $table) {
            $table->foreign('brand_id')
                ->references('id')->on('brands')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brands_translations');
        Schema::dropIfExists('brands');
    }
}
