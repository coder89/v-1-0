<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('areas', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('status')->default('1')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('areas_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('slug');
            $table->text('description')->nullable();
            $table->unsignedInteger('area_id');
            $table->string('locale')->index();
            $table->unique(['area_id','locale']);
        });
        Schema::table('areas_translations', function (Blueprint $table) {
            $table->foreign('area_id')
                ->references('id')->on('areas')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('areas_translations');
        Schema::dropIfExists('areas');
    }
}
