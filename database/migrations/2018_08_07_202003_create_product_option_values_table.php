<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductOptionValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_option_values', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_option_id');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('product_option_values_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_option_value_id');
            $table->string('locale')->index();
            $table->string('value');
            $table->unique(['product_option_value_id','locale'], 'option_values_translations_id_locale_unique');
        });
        Schema::table('product_option_values', function (Blueprint $table) {
            $table->foreign('product_option_id')
                ->references('id')->on('product_options')
                ->onDelete('cascade');
        });
        Schema::table('product_option_values_translations', function (Blueprint $table) {
            $table->foreign('product_option_value_id', 'option_value_translation_id_foreign')
                ->references('id')->on('product_option_values')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_option_values_translations');
        Schema::dropIfExists('product_option_values');
    }
}
