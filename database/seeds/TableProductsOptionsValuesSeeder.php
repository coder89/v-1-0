<?php

use App\Product_option_value;
use Illuminate\Database\Seeder;

class TableProductsOptionsValuesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {
            Product_option_value::create([
                'product_option_id' => $faker->numberBetween(1,10),
            ]);
        }
    }
}
