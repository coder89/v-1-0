<?php

use Illuminate\Database\Seeder;
use App\Areas_translation;
class TableAreaTrnslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i=0; $i < 10; $i++) {
            Areas_translation::create([
                'name' => $faker->name,
                'slug' => $faker->name,
                'description' => $faker->realText(200),
                'area_id' => $faker->numberBetween(1,10),
                'locale' => "en",

            ]);
        }
    }
}
