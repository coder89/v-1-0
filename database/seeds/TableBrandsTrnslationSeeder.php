<?php

use App\Brands_translation;
use Illuminate\Database\Seeder;

class TableBrandsTrnslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i=0; $i < 10; $i++) {
            Brands_translation::create([
                'name' => $faker->name,
                'slug' => $faker->name,
                'manufacturer' => $faker->realText(200),
                'short_description' => $faker->realText(30),
                'description' => $faker->realText(200),
                'brand_id' =>  $faker->numberBetween(1,10),
            ]);
        }
    }
}
