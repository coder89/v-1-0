<?php

use App\Category;
use Illuminate\Database\Seeder;

class TableCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {
            Category::create([
                'status' => $faker->numberBetween(1, 10),
                'family_id' => $faker->numberBetween(1, 10),
                'avatar' => "https://ssl-product-images.www8-hp.com/digmedialib/prodimg/lowres/c05943533.png",
            ]);
        }

    }
}
