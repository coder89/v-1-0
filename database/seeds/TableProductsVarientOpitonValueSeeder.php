<?php

use App\Product_variant_option_value;
use Illuminate\Database\Seeder;

class TableProductsVarientOpitonValueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {

            Product_variant_option_value::create([
                'product_variant_id' => $faker->numberBetween(1,10),
                'product_option_value_id' => $faker->numberBetween(1,10),
            ]);
        }
    }
}
