<?php

use App\Uom;
use Illuminate\Database\Seeder;

class TableUnitMeassure extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {

            Uom::create([
                'name' => $faker->text,
            ]);
        }
    }
}
