<?php

use App\Product_properties_translation;
use Illuminate\Database\Seeder;

class TableProductsPropertiesTransaltionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {
            Product_properties_translation::create([
                'property_id' => $faker->numberBetween(1,10),
                'locale' => "en",
                'presentation' => $faker->name,
            ]);
        }
    }
}
