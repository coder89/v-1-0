<?php

use App\Product;
use Illuminate\Database\Seeder;

class TableProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {

            Product::create([
                'sku' => $faker->numberBetween(1,10),
                'uom_id' => $faker->numberBetween(1,10),
                'brand_id' => $faker->numberBetween(1,10),
                'avatar' => $faker->random(),
                'avatars' => $faker->random(),
                'status' => $faker->numberBetween(1,0),
                'available_on' => $faker->dateTime(10,+02),
            ]);
        }

    }
}
