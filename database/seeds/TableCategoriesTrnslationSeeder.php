<?php

use App\Categories_translation;
use App\Category;
use Illuminate\Database\Seeder;

class TableCategoriesTrnslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i=0; $i < 10; $i++) {
            Categories_translation::create([
                'name' => $faker->name,
                'slug' => $faker->name,
                'short_description' => $faker->realText(30),
                'description' => $faker->realText(200),
                'category_id' =>  $faker->numberBetween(1,10),
                'locale' => $faker->boolean("en","ar"),

            ]);
        }
    }
}
