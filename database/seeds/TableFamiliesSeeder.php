<?php

use App\Family;
use Illuminate\Database\Seeder;

class TableFamiliesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {
            Family::create([
                'status' => 1,
                'avatar' => "https://ssl-product-images.www8-hp.com/digmedialib/prodimg/lowres/c05943533.png",
            ]);
        }
    }
}
