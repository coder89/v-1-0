<?php

use App\Product_option_values_translation;
use Illuminate\Database\Seeder;

class TableProductsOptionsValuesTrnaslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {
            Product_option_values_translation::create([
                'product_option_value_id' => $faker->numberBetween(1,10),
                'locale' => "en",
                'value' => $faker->text,
            ]);
        }
    }
}
