<?php

use App\Brand;
use Illuminate\Database\Seeder;

class TableBrandsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i=0; $i < 10; $i++) {
            Brand::create([
                'avatar' => "https://ssl-product-images.www8-hp.com/digmedialib/prodimg/lowres/c05943533.png",
                'status' => "active",
            ]);
        }
    }
}
