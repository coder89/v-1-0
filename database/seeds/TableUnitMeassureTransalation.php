<?php

use App\Uoms_translation;
use Illuminate\Database\Seeder;

class TableUnitMeassureTransalation extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {

            Uoms_translation::create([
                'presentation' => $faker->text,
            ]);
        }
    }
}
