<?php

use App\Product_option;
use Illuminate\Database\Seeder;

class TableProductsOptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {
            Product_option::create([
                'name' => $faker->text,
                'product_id' => $faker->numberBetween(1,10),
                'sort_order' => 1,
                'is_required' => 1,
                'is_hidden' => 0,
            ]);
        }
    }
}
