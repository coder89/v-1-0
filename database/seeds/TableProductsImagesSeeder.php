<?php

use App\Product_image;
use Illuminate\Database\Seeder;

class TableProductsImagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {
            Product_image::create([
                'product_variant_id' => $faker->numberBetween(1, 10),
                'path' => "/public/Products",
            ]);
        }

    }
}
