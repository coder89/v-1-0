<?php

use App\Product_options_translation;
use Illuminate\Database\Seeder;

class TableProductsOptionsTrnanslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {
            Product_options_translation::create([
                'product_option_id' => $faker->numberBetween(1,10),
                'locale' => "en",
                'presentation' => $faker->text,
            ]);
        }
    }
}
