<?php

use Illuminate\Database\Seeder;
use App\Area;

class TableAreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i=0; $i < 10; $i++) {
            Area::create([
                'status' => $faker->numberBetween(1,10),

            ]);
        }

    }
}
