<?php

use App\Product;
use App\Products_translation;
use Illuminate\Database\Seeder;

class TableProductsTrnslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {

            Products_translation::create([
                'name' => $faker->text,
                'product_id' => $faker->numberBetween(1,0),
                'locale' => "en",
                'slug' => $faker->text,
                'short_description' => $faker->text(100),
                'description' => $faker->realText(200),
                'available_on' => $faker->dateTime(10,+2),
            ]);
        }
    }
}
