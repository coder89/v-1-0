<?php

use App\Families_translation;
use Illuminate\Database\Seeder;

class TableFamiliesTrnslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {
            Families_translation::create([
                'family_id' => $faker->numberBetween(1, 10),
                'name' => $faker->name,
                'slug' => $faker->name,
                'locale' => $faker->random("en","ar"),
            ]);
        }
    }
}
