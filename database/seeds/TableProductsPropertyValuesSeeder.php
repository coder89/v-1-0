<?php

use App\Product_property_value;
use Illuminate\Database\Seeder;

class TableProductsPropertyValuesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {
            Product_property_value::create([
                'product_id' => $faker->numberBetween(1,10),
                'property_id' => $faker->numberBetween(1,10),
                'value' => $faker->name,
            ]);
        }
    }
}
