<?php

use App\Product_property;
use Illuminate\Database\Seeder;

class TableProductsPropertiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {
            Product_property::create([
                'name' => $faker->text,
            ]);
        }
    }
}
