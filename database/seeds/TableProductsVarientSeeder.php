<?php

use App\Product_variant;
use Illuminate\Database\Seeder;

class TableProductsVarientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i = 0; $i < 10; $i++) {
            Product_variant::create([
                'name' => $faker->name,
                'sku' => $faker->name,
                'master' => $faker->numberBetween(1,10),
                'on_hand' => $faker->numberBetween(1,3),
                'status' => $faker->numberBetween(1,0),
                'weight' => $faker->numberBetween(10,300),
                'width' => $faker->numberBetween(10,300),
                'height' => $faker->numberBetween(10,300),
                'depth' => $faker->numberBetween(10,300),
                'product_id' => $faker->numberBetween(1,10),
                'available_on_demand' => $faker->numberBetween(1,0),
            ]);
        }
    }
}
