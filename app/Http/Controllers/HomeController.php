<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Xaamin\Whatsapi\Facades\Native\Whatsapi;
use Xaamin\Whatsapi\Facades\Native\WhatsapiTool;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      //  $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function whats(){

        $number = '+201091950488';
        $code = '132456'; # Replace with received code
        $response = WhatsapiTool::registerCode($number, $code);

        $result = Whatsapi::syncContacts(['+201091950488', '+201284073675']);

        $avaliable = $result->existing;

        return response()->json(["result" => $response]);
    }
}

