<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class POptionValueApi extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "product_option_id" => $this->product_option_id,
            "value" => $this->value,
        ];
    }
}
