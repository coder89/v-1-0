<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ShipingClassesAPi extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "status" => $this->status,
            "added_value_rule" => $this->added_value_rule,
            "added_value_fixed" => $this->added_value_fixed,
            "added_percentage" => $this->added_percentage,
            "description" => $this->description,
        ];
    }
}
