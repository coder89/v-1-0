<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductVarientApi extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "master" => $this->master,
            "name" => $this->name,
            "sku" => $this->sku,
            "on_hand" => $this->on_hand,
            "status" => $this->status,
            "available_on_demand" => $this->available_on_demand,
            "weight" => $this->weight,
            "width" => $this->width,
            "height" => $this->height,
            "depth" => $this->depth,
        ];
    }
}
