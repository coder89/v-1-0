<?php

namespace App\Http\Resources;

use App\Family;
use Illuminate\Http\Resources\Json\JsonResource;

class categoriesApi extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'status' => $this->status,
            'Image' => $this->avatar,
            'name' => $this->name,
            'slug' => $this->slug,
            'description' => $this->description,
            'Family_group' => $this->Family,
            'Language_Family' => $this->Family->Families_translation,
        ];
    }
}
