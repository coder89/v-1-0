<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BrandsApi extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'avatar' => $this->avatar,
            'status' => $this->status,
            'name' => $this->name,
            'slug' => $this->slug,
            'manufacturer' => $this->manufacturer,
            'description' => $this->description,
        ];
    }
}
