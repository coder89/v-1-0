<?php

namespace App\Http\Resources;

use App\Area;
use Illuminate\Http\Resources\Json\JsonResource;

class AreasApi extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Area $Area
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'status' => $this->status,
            'name' =>$this->name,
            'description' =>$this->description,
            'slug' =>$this->slug,
        ];
    }
}
