<?php

namespace App\Http\Resources;

use App\ProductOption;
use App\ProductOptionValue;
use App\ProductVariant;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductsApi extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => (integer)$this->id,
            "sku" => (string)$this->sku,
            "name" => (string)$this->name,
            "slug" => $this->slug,
            'Unit Of Measure' => $this->Unit->name,
            "description" => $this->description,
            "Brand" => $this->Brands->name,
            "Category" => $this->Category,
            "Brand_status" => $this->Brands->status,
            "Brand_Slug" => $this->Brands->slug,
            "Original Picture" => $this->avatar,
            "status" => $this->status,
            "Available In" => $this->available_on,
            "Options" => POptionApi::collection(ProductOption::with("Products")->get()),
            "Value" => POptionValueApi::collection(ProductOptionValue::with("Option")->get()),
            "Product Varient" => ProductVarientApi::collection(ProductVariant::with("Pro_varient")->get()),
            "Images" => ProductVarientApi::collection(ProductVariant::with("Images")->get()),
        ];
    }
}
