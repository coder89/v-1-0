<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class POptionApi extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "presentation" => $this->presentation,
            "sort_order" => $this->sort_order,
            "is_required" => $this->is_required,
            "is_hidden" => $this->is_hidden,
        ];
    }
}
