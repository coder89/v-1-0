<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ShipingCostsAPi extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "status" => $this->status,
            "cost" => $this->cost,
            "description" => $this->description,
            "shipping class" => $this->Shipping->name,
            "shipping price" => $this->Shipping->added_value_fixed,
            "shipping Extra Fees" => $this->Shipping->added_percentage,
        //    "area" => $this->area->name,
            "agent" => $this->agent->name,
            "start in" => $this->start_date,
            "end in" => $this->end_date,
        ];
    }
}
