<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplierContact extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'supplier_contacts';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'supplier_id', 'phone', 'mobile', 'email', 'status', 'description', 'deleted_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = ['status' => 'boolean'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'deleted_at', 'deleted_at', 'deleted_at', 'deleted_at', 'deleted_at', 'deleted_at', 'deleted_at', 'expires_at', 'expires_at', 'expires_at', 'deleted_at', 'deleted_at', 'deleted_at', 'available_on', 'deleted_at', 'deleted_at', 'deleted_at', 'deleted_at', 'start_date', 'end_date', 'deleted_at', 'deleted_at', 'start_date', 'end_date', 'deleted_at', 'deleted_at'];

}