<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingAgent extends Model  {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shipping_agents';


    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'address', 'status', 'description', 'deleted_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = ['status' => 'boolean'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['expires_at','deleted_at', 'available_on'];


    public function contacts(){

        return $this->hasMany(ShippingAgentContact::class);
    }

    public function Classes(){

        return $this->belongsToMany(ShippingClass::class);
    }

    public function Cost(){

        return $this->hasMany(ShippingCost::class);
    }


}