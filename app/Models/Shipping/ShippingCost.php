<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingCost extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shipping_costs';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'shipping_class_id', 'store_area_id', 'area_id', 'shipping_agent_id', 'start_date', 'cost', 'end_date', 'status', 'description', 'deleted_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = ['status' => 'boolean'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['expires_at', 'deleted_at', 'start_date', 'end_date'];

    public function Shipping(){

        return $this->belongsTo(ShippingClass::class,"shipping_class_id");
    }
    public function area(){
        return $this->belongsTo(Area::class);
    }

    public function agent(){
        return $this->belongsTo(ShippingAgent::class,"shipping_agent_id");
    }



}