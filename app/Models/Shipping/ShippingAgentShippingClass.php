<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingAgentShippingClass extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shipping_agent_shipping_class';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['shipping_agent_id', 'shipping_class_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at','expires_at','available_on'];

    public function Agent_class(){

        return $this->morphToMany("shipping_agent_id","id",ShippingAgent::class,"shipping_agent_id");

    }
}