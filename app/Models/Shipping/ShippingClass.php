<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingClass extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shipping_classes';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'status', 'added_value_rule', 'added_value_fixed', 'added_percentage', 'description', 'deleted_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = ['status' => 'boolean'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['expires_at','available_on'];

    public function Agent(){
        return $this->belongsToMany(ShippingAgent::class);
    }
    public function Costs(){
        return $this->hasMany(ShippingCost::class);
    }
}