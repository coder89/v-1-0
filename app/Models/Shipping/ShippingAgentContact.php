<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingAgentContact extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shipping_agent_contacts';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'shipping_agent_id', 'phone', 'mobile', 'email', 'status', 'description', 'deleted_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = ['status' => 'boolean'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at','expires_at','available_on'];


    public function Agent(){

        return $this->belongsTo(ShippingAgent::class);

    }

}