<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreArea extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'store_areas';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'status', 'description', 'deleted_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = ['status' => 'boolean'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['expires_at', 'deleted_at', 'available_on','start_date', 'end_date'];

}