<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreProductVariant extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'store_product_variant';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['product_variant_id', 'store_id', 'on_hand', 'status', 'available_on_demand', 'price', 'start_date', 'end_date', 'description'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = ['status' => 'boolean', 'available_on_demand' => 'boolean'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at','start_date', 'end_date'];

}