<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'stores';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'address', 'status', 'store_area_id', 'supplier_id', 'description', 'deleted_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = ['status' => 'boolean'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'expires_at','available_on', 'deleted_at', 'start_date', 'end_date'];

    public function area(){

        return $this->belongsTo(Area::class,"store_area_id");
    }
    public function buyer(){

        return $this->belongsTo(Supplier::class,"supplier_id");
    }

}