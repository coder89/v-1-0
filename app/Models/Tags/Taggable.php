<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Taggable extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'taggables';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['tag_id', 'taggable_id', 'taggable_type'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'deleted_at', 'deleted_at', 'deleted_at', 'deleted_at', 'deleted_at', 'deleted_at', 'deleted_at', 'expires_at', 'expires_at', 'expires_at', 'deleted_at', 'deleted_at', 'deleted_at', 'available_on', 'deleted_at', 'deleted_at', 'deleted_at', 'deleted_at', 'start_date', 'end_date', 'deleted_at', 'deleted_at', 'start_date', 'end_date', 'deleted_at', 'deleted_at', 'deleted_at', 'deleted_at'];

}