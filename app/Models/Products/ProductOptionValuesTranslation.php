<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductOptionValuesTranslation extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_option_values_translations';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['product_option_value_id', 'locale', 'value'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at','expires_at'];

    public function Value(){

        return $this->belongsTo(ProductOptionValue::class);
    }

    public $timestamps = false;

}