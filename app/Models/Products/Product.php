<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model  {

    use SoftDeletes, Translatable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';
    public $useTranslationFallback = true;
    public $translationModel = ProductsTranslation::class;
    public $translatedAttributes = ['name', 'slug', 'short_description', 'description'];


    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['sku', 'uom_id', 'brand_id', 'avatar', 'avatars', 'status', 'available_on', 'deleted_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'expires_at', 'available_on'];

    public function Brands(){

        return $this->belongsTo(Brand::class,"brand_id");
    }
    public function Options(){

        return $this->hasMany(ProductOption::class);
    }
    public function Varient(){

        return $this->hasMany(ProductVariant::class);
    }

    public function Unit(){
        return $this->belongsTo(Uom::class,'uom_id');
    }

    public function Category(){

        return $this->hasMany(CategoryProduct::class,"product_id");
    }

    public function lang(){

        return $this->hasMany(ProductsTranslation::class,"product_id");
    }



    protected $with = ['translations'];



}