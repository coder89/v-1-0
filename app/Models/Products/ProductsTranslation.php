<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductsTranslation extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products_translations';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'product_id', 'locale', 'slug', 'short_description', 'description'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['expires_at','deleted_at', 'available_on'];

    public function Products(){

        return $this->belongsTo(Product::class);
    }

}