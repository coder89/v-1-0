<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductOption extends Model  {

    use SoftDeletes, Translatable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_options';
    public $useTranslationFallback = true;
    public $translationModel = ProductOptionsTranslation::class;
    public $translatedAttributes = ['product_option_id', 'locale', 'presentation'];


    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'product_id', 'sort_order', 'is_required', 'is_hidden', 'deleted_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = ['is_required' => 'boolean', 'is_hidden' => 'boolean'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [ 'expires_at','deleted_at'];

    public function Value(){

        return $this->hasMany(ProductOptionValue::class);
    }
    public function Products(){

        return $this->belongsTo(Product::class);
    }
    protected $with = ['translations'];


    public $timestamps = false;

}