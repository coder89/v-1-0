<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVariant extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_variants';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['master', 'name', 'sku', 'on_hand', 'status', 'available_on_demand', 'weight', 'width', 'height', 'depth', 'product_id', 'deleted_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = ['master' => 'boolean', 'status' => 'boolean', 'available_on_demand' => 'boolean'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['expires_at','deleted_at'];

    public function Pro_varient(){

        return $this->belongsTo(Product::class);
    }
    public function Images(){

        return $this->belongsTo(ProductImage::class,"product_variant_id","id");
    }
}