<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'customers';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'address', 'status', 'description', 'user_id', 'customer_type_id', 'deleted_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = ['status' => 'boolean'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    public function users(){

        return $this->belongsTo(User::class,"user_id");
    }
    public function type(){

        return $this->belongsTo(CustomerType::class,"customer_type_id");
    }
    public function details(){

        return $this->hasMany(CustomerAddress::class,"customer_id");
    }
    public function contacts(){

        return $this->hasMany(CustomerContact::class,"customer_id");

    }
}