<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Parent_;

class CustomerAddress extends Model  {

    public function __construct()
    {
        $this->timestamps = false;
    }

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'customer_addresses';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['area_id', 'customer_id', 'address', 'is_default', 'description', 'deleted_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = ['is_default' => 'boolean'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function area(){

        return $this->belongsTo(Area::class,"area_id");
    }
    public function customers(){

        return $this->belongsTo(Customer::class);
    }



}