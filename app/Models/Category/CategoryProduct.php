<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryProduct extends Model  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'category_product';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['category_id', 'product_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function Category_Product(){

        return $this->morphToMany("product_id","id",Product::class,"product_id");

    }

}