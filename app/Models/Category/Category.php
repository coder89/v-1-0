<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model  {

    use SoftDeletes, Translatable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';
    public $useTranslationFallback = true;
    public $translationModel = CategoriesTranslation::class;
    public $translatedAttributes = ['name', 'slug', 'short_description', 'description'];


    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['status', 'family_id', 'avatar', 'deleted_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function Family(){

       return $this->belongsTo(Family::class);
    }

    public function Products(){

        return $this->hasMany(CategoryProduct::class,"category_id");
    }

    protected $with = ['translations'];


}