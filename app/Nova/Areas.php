<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Panel;
use MrMonat\Translatable\Translatable;

class Areas extends Resource
{

    public static function label()
    {
        return __('nova.Area');
    }

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Area';


    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name','slug'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),
            ID::make('area_id')
                ->hideWhenUpdating()
                ->hideWhenCreating()
                ->hideFromIndex()
                ->hideFromDetail(),
            Translatable::make('Name',"name")
                ->rules("required","max:255")
                ->locales([
                    'en' => 'English',
                    'ar' => 'Arabic',
                ])
                ->indexLocale('en')
                ->indexLocale('ar')
                ->sortable(),
            Translatable::make('Description',"description")
                ->rules("required")
                ->locales([
                    'en' => 'English',
                    'ar' => 'Arabic',
                ])
                ->indexLocale('en')
                ->indexLocale('ar')
                ->sortable()
                ->trix(),
            Translatable::make('Slug',"slug")
                ->rules("required","max:50")
                ->locales([
                    'en' => 'English',
                    'ar' => 'Arabic',
                ])
                ->indexLocale('en')
                ->indexLocale('ar')
                ->sortable(),
            new Panel('Extra', $this->visibilityFields()),

        ];
    }
    public function visibilityFields()
    {
        return [
            Select::make('Status',"status")->options([
                1 => 'Active',
                0 => 'Disactive',
            ]),
        ];
    }


    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [

        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [

        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
